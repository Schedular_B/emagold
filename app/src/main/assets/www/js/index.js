/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

// Wait for the deviceready event before using any of Cordova's device APIs.
// See https://cordova.apache.org/docs/en/latest/cordova/events/events.html#deviceready
document.addEventListener('deviceready', onDeviceReady, false);

function checkConnection() {
  var networkState = navigator.network.connection.type;
  var states = {};
  states[Connection.UNKNOWN] = 'Unknown connection';
  states[Connection.ETHERNET] = 'Ethernet connection';
  states[Connection.WIFI] = 'WiFi connection';
  states[Connection.CELL_2G] = 'Cell 2G connection';
  states[Connection.CELL_3G] = 'Cell 3G connection';
  states[Connection.CELL_4G] = 'Cell 4G connection';
  states[Connection.NONE] = 'No network connection';
  return networkState;
}  

function onDeviceReady() {
    // Cordova is now initialized. Have fun!

	screen.orientation.lock('portrait');
	ons.fastClick; // prevent browser delay
	var modal = document.querySelector('ons-modal'); // loading modal
	document.addEventListener("backbutton", onBackKeyDown, false); // hardware back button
    var networkState = checkConnection();
	 /* load local files if there is not network connection */
	if (networkState == Connection.NONE) {
		window.location="index.html";
	}else
	{
		document.getElementById('cosmetics').addEventListener('click', function() {
			var ref = getWebContent('https://emagld.com/11-cosmetics-and-perfume');
			getInappEvent(ref);
		});
		document.getElementById('fashion').addEventListener('click', function() {
			var ref = getWebContent('https://emagld.com/10-fashion');
			getInappEvent(ref);
		});
		document.getElementById('hairline').addEventListener('click', function() {
			var ref = getWebContent('https://emagld.com/9-hair-line');
			getInappEvent(ref);
		});
		document.getElementById('dryclean').addEventListener('click', function() {
			var ref = getWebContent('https://emagld.com/6-dry-cleaning');
			getInappEvent(ref);
		});
		document.getElementById('ebite').addEventListener('click', function() {
			var ref = getWebContent('https://emagld.com/3-ebites');
			getInappEvent(ref);
		});
		document.getElementById('grocery').addEventListener('click', function() {
			
			var ref = getWebContent('https://emagld.com/4-grocery');
			getInappEvent(ref);
		});
		document.getElementById('makehair').addEventListener('click', function() {
			var ref = getWebContent('https://emagld.com/7-hair-line');
			getInappEvent(ref);
		});
		document.getElementById('makeup').addEventListener('click', function() {
			var ref = getWebContent('https://emagld.com/8-make-up');
			getInappEvent(ref);
		});
		//bottom menu
		document.getElementById('home').addEventListener('click', function() {
			var ref = getWebContent('https://emagld.com');
			getInappEvent(ref);
		});
		document.getElementById('tandc').addEventListener('click', function() {
			var ref = getWebContent('https://emagld.com/content/3-terms-and-conditions-of-use');
			getInappEvent(ref);
		});
	}
	
	document.getElementById('deviceready').classList.add('ready');
	
	
}

//Navigation for onsenui
document.addEventListener('init', function(event) {
  var page = event.target;

  if (page.id === 'index') {
    page.querySelector('#info').onclick = function() {
      document.querySelector('#pgNavigator').pushPage('about.html', {data: {title: 'About'}});
    };
	page.querySelector('#stores').onclick = function() {
      document.querySelector('#pgNavigator').pushPage('stores.html', {data: {title: 'Stores'}});
    };
  } else if (page.id === 'about') {
    page.querySelector('ons-toolbar .center').innerHTML = page.data.title;
  } else if (page.id === 'stores') {
    page.querySelector('ons-toolbar .center').innerHTML = page.data.title;
  }
  //stores
	document.querySelector('#stores #jos-menu').onclick = function() { 
		document.querySelector('#pgNavigator').pushPage('jos.html', {data: {title: 'Jos'}}); // load menu for jos store
	};
	//links menu for jos
	document.querySelector('#jos #cosmetics').onclick = function() {
		var ref = getWebContent('https://emagld.com/jos/11-cosmetics-and-perfume');
		getInappEvent(ref);
	};
	document.querySelector('#jos #fashion').onclick = function() {
		var ref = getWebContent('https://emagld.com/jos/10-fashion');
		getInappEvent(ref);
	};
	document.querySelector('#jos #hairline').onclick = function() {
		var ref = getWebContent('https://emagld.com/jos/9-hair-line');
		getInappEvent(ref);
	};
		document.querySelector('#jos #dryclean').onclick = function() {
		var ref = getWebContent('https://emagld.com/jos/6-dry-cleaning');
		getInappEvent(ref);
	};
	document.querySelector('#jos #ebite').onclick = function() {
		var ref = getWebContent('https://emagld.com/jos/3-ebite');
		getInappEvent(ref);
	};
	document.querySelector('#jos #grocery').onclick = function() {
		var ref = getWebContent('https://emagld.com/jos/4-grocery');
		getInappEvent(ref);
	};
	document.querySelector('#jos #makehair').onclick = function() {
		var ref = getWebContent('https://emagld.com/jos/7-hair-line');
		getInappEvent(ref);
	};
	document.querySelector('#jos #makeup').onclick = function() {
		var ref = getWebContent('https://emagld.com/jos/8-make-up');
		getInappEvent(ref);
	};
	document.querySelector('#jos #home').onclick = function() {
		var ref = getWebContent('https://emagld.com/jos/');
		getInappEvent(ref);
	};
	//toolbar
	document.querySelector('#jos #main_menu').onclick = function() {
		window.location = 'index.html';
	};
});

function getWebContent(webContent){
	return cordova.InAppBrowser.open(webContent, '_blank',"hideurlbar=yes,zoom=no,hidenavigationbuttons=yes,hidden=yes,hideurlbar=yes,toolbarcolor=#8c4b00");
}

function getInappEvent(evt){
	var loadChecker = setTimeout(function(){ 
		evt.close();
		modal.hide();
		ons.notification.toast('Emagold Busy Try Again!', { timeout: 2500, animation: 'default' });	
	   }, 15000);
	modal.show();
	//evt.addEventListener('loadstart', showProg);
	evt.addEventListener('loaderror', function(){
		evt.close();
		modal.hide();
		ons.notification.toast('An Error oocured oR Internet not available!', { timeout: 2500, animation: 'default' });
		clearTimeout(loadChecker);
	});
	evt.addEventListener('loadstop', function(){
		evt.show();
		modal.hide();
		clearTimeout(loadChecker);
	});
	loadChecker;
}

var modal = document.querySelector('ons-modal');
	
function showProg(){
	//modal.show();
	//ons.notification.toast('Please wait', { timeout: 1000, animation: 'fall' });
}

function onBackKeyDown() {
	var appNav = document.querySelector('#pgNavigator');   
	if (appNav.pages.length > 1) {
        appNav.popPage();
    } else {
        ons.notification.confirm('Do you want to close the app?') // Ask for confirmation
				.then(function(index) {
			    if (index === 1) { // OK button
					navigator.app.exitApp(); // Close the app
				  }
		});
    }
}